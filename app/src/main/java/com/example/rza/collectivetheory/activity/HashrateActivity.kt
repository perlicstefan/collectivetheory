package com.example.rza.collectivetheory.activity

import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.example.rza.collectivetheory.R
import com.example.rza.collectivetheory.apiKeySharedPreference
import com.example.rza.collectivetheory.responseInvalidToken
import com.example.rza.collectivetheory.responseOk
import com.example.rza.collectivetheory.usecase.AccountDaoUseCase
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_threshold.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HashrateActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_threshold)

        val apiKey = PreferenceManager.getDefaultSharedPreferences(this).getString(apiKeySharedPreference, "")

        progress.visibility = View.VISIBLE
        GlobalScope.launch(Dispatchers.IO) {
            val accountUseCase = AccountDaoUseCase()
            val call = accountUseCase.getAccount(apiKey).execute()
            val response = call.code()
            when (response) {
                responseOk -> {
                    val account = call.body()
                    withContext(Dispatchers.Main) {
                        hashrate.text = account?.toPetahashes()?.format()
                        progress.visibility = View.INVISIBLE
                        Toasty.success(this@HashrateActivity, "Api Key exists!", Toast.LENGTH_LONG).show()
                    }
                }
                responseInvalidToken -> Toasty.error(this@HashrateActivity, "Wrong api key", Toast.LENGTH_LONG).show()
                else -> Toasty.error(this@HashrateActivity, "Something went wrong", Toast.LENGTH_LONG).show()
            }
        }
    }
}
