package com.example.rza.collectivetheory.activity

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.os.Handler
import android.preference.PreferenceManager
import android.util.Log
import android.widget.RemoteViews
import android.widget.Toast
import com.example.rza.collectivetheory.NotificationTray
import com.example.rza.collectivetheory.R
import com.example.rza.collectivetheory.apiKeySharedPreference
import com.example.rza.collectivetheory.thresholdSharedPreference
import com.example.rza.collectivetheory.usecase.AccountDaoUseCase
import es.dmoral.toasty.Toasty
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.net.SocketTimeoutException

/**
 * Implementation of App Widget functionality.
 */
class HashrateWidget : AppWidgetProvider() {
    private val fiveMinutes = 300000L
    private val halfAMinute = 30000L

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {

        updateWidget(context, appWidgetManager, appWidgetIds)
        val handler = Handler()
        val delay = fiveMinutes
        handler.postDelayed(object : Runnable {
            override fun run() {
                updateWidget(context, appWidgetManager, appWidgetIds)
                handler.postDelayed(this, delay)
            }
        }, delay)
    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
    }

    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    companion object {

        internal fun updateAppWidget(
            context: Context,
            appWidgetManager: AppWidgetManager,
            appWidgetId: Int,
            hashrateString: String?
        ) {
            val views = RemoteViews(context.packageName, R.layout.hashrate_widget)
            views.setTextViewText(R.id.hashrate, hashrateString)

            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
    }

    private fun updateWidget(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        val useCase = AccountDaoUseCase()
        val apiKey =
            PreferenceManager.getDefaultSharedPreferences(context).getString(apiKeySharedPreference, "none")
        try {
            GlobalScope.launch(Dispatchers.IO) {
                val account = useCase.getAccount(apiKey).execute().body()
                launch(Dispatchers.Main) {
                    for (appWidgetId in appWidgetIds) {
                        updateAppWidget(context, appWidgetManager, appWidgetId, account?.toPetahashes()?.format())
                    }
                    Log.d("updated", "true")
                    val threshold = PreferenceManager.getDefaultSharedPreferences(context)
                        .getFloat(thresholdSharedPreference, 30.00F)
                    if (account?.toPetahashes()?.hashrate!! < threshold) {
                        val notificationTray = NotificationTray()
                        notificationTray.startNotification(context, account.toPetahashes().format())
                    }
                }
            }
        } catch (exc: SocketTimeoutException) {
            Toasty.error(context, "Server timeout!", Toast.LENGTH_LONG).show()
        }

    }
}

