package com.example.rza.collectivetheory.dao

import com.example.rza.collectivetheory.dao.dto.HashrateDto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path


interface ApiService {
    @GET("{apiKey}")
    fun getHashes(@Path("apiKey") apiKey: String): Call<HashrateDto>
}