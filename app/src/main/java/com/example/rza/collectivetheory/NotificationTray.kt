package com.example.rza.collectivetheory

import android.content.Context
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat

class NotificationTray {
    fun startNotification(context: Context, hashrate: String?) {
        val notification = NotificationCompat.Builder(context, context.getString(R.string.notification_channel))
            .setSmallIcon(R.drawable.image)
            .setContentTitle("Collective Theory")
            .setContentText("Your hashrate is: $hashrate")
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .build()

        with(NotificationManagerCompat.from(context)) {
            notify(0, notification)
        }
    }
}