package com.example.rza.collectivetheory.usecase

import com.example.rza.collectivetheory.dao.RetrofitClientImpl
import com.example.rza.collectivetheory.dao.dto.HashrateDto
import retrofit2.Call

class AccountDaoUseCase {
    fun getAccount(apiKey: String): Call<HashrateDto> {
        return RetrofitClientImpl.apiService.getHashes(apiKey)
    }
}