package com.example.rza.collectivetheory

const val thresholdSharedPreference = "threshold"
const val apiKeySharedPreference = "apiKey"
const val responseOk = 200
const val responseInvalidToken = 401