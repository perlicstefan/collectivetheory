package com.example.rza.collectivetheory.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.Toast
import com.example.rza.collectivetheory.R
import com.example.rza.collectivetheory.thresholdSharedPreference
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : AppCompatActivity() {
    private val thresholdExtra = "threshold"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

      val thresholdFloat = PreferenceManager.getDefaultSharedPreferences(this).getFloat(
          thresholdSharedPreference, 10.00F)

        threshold.setText(thresholdFloat.toString())

        submitSettings.setOnClickListener {
            val value = threshold.text.toString().toFloat()
            PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putFloat(thresholdExtra, value).apply()
            threshold.setText("")
            val thresholdText = "%.2f".format(value)
            Toasty.success(this, "You set the threshold to $thresholdText", Toast.LENGTH_LONG).show()
            val intent = Intent(this, HashrateActivity::class.java)
            startActivity(intent)
        }
    }
}
