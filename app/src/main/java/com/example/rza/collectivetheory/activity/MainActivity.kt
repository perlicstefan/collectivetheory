package com.example.rza.collectivetheory.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import com.example.rza.collectivetheory.R
import kotlinx.android.synthetic.main.activity_main.*
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import com.example.rza.collectivetheory.apiKeySharedPreference


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val apiKey = PreferenceManager.getDefaultSharedPreferences(this).getString(
            apiKeySharedPreference, "")
        apiKeyText.setText(apiKey)


        submit.setOnClickListener {
            updateWidget()
            val intent = Intent(this@MainActivity, HashrateActivity::class.java)
            PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putString(apiKeySharedPreference, apiKeyText.text.toString()).apply()
            startActivity(intent)
        }
    }

    private fun updateWidget() {
        val intent = Intent(this, HashrateWidget::class.java)
        intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
        val ids = AppWidgetManager.getInstance(application)
            .getAppWidgetIds(ComponentName(application, HashrateWidget::class.java))
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
        sendBroadcast(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = MenuInflater(this)
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.settings -> {
                openSettingsActivity()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }

    private fun openSettingsActivity() {
        val intent = Intent(this, SettingsActivity::class.java)
        startActivity(intent)
    }


    //TODO notification if below a certain threshold - DONE
    //TODO save api key in shared preferences - DONE
    //TODO enable user to enter a threshold - DONE
    //TODO save the threshold in shared preferences - DONE
    //TODO convert to petahashes - DONE
    //TODO define default value for threshold - DONE
}
