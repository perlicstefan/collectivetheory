package com.example.rza.collectivetheory.dao.dto

class HashrateDto(val hashrate: Float) {

    fun toPetahashes(): HashrateDto {
        return HashrateDto(hashrate / 1_000_000_000)
    }

    fun format(): String {
        return "%.2f".format(hashrate)
    }
}